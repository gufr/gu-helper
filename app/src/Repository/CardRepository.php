<?php

namespace App\Repository;

use App\Entity\Card;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Card|null find($id, $lockMode = null, $lockVersion = null)
 * @method Card|null findOneBy(array $criteria, array $orderBy = null)
 * @method Card[]    findAll()
 * @method Card[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Card::class);
    }

    public function updateLastPrice($user): void
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            UPDATE `card` c, `last_sale` s
SET c.`last_offer_price`= s.`eth_price`
WHERE c.user = :user 
  AND s.proto = c.unique_card_id AND s.quality = c.quality
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['user' => $user]);
    }

    public function deleteByUser($user)
    {
        return $this->createQueryBuilder('c')
            ->delete()
            ->where('c.user = :usr')
            ->setParameter(':usr', $user)
            ->getQuery()
            ->execute();
    }

    // /**
    //  * @return Card[] Returns an array of Card objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Card
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
