<?php

namespace App\Repository;

use App\Entity\LastSale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LastSale|null find($id, $lockMode = null, $lockVersion = null)
 * @method LastSale|null findOneBy(array $criteria, array $orderBy = null)
 * @method LastSale[]    findAll()
 * @method LastSale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LastSaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LastSale::class);
    }

}
