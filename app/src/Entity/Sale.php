<?php

namespace App\Entity;

use App\Repository\SaleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SaleRepository::class)
 * @ORM\Table(indexes={@ORM\Index(name="search_idx", columns={"proto", "quality"})})
 */
class Sale
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $proto;

    /**
     * @ORM\Column(type="smallint")
     */
    private $quality;

    /**
     * @ORM\Column(type="float")
     */
    private $eth_price;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $sold_at;

    private $decimals;

    public function __construct()
    {
        $this->created_at = new \DateTimeImmutable();
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProto(): ?int
    {
        return $this->proto;
    }

    public function setProto(int $proto): self
    {
        $this->proto = $proto;

        return $this;
    }

    public function getQuality(): ?int
    {
        return $this->quality;
    }

    public function setQuality(int $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    public function getEthPrice(): ?float
    {
        return $this->eth_price;
    }

    public function setEthPrice(float $eth_price): self
    {
        $this->eth_price = $eth_price / pow(10, $this->getDecimals());

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getDecimals()
    {
        return $this->decimals ?: 18;
    }

    /**
     * @param mixed $decimals
     */
    public function setDecimals($decimals): void
    {
        $this->decimals = $decimals;
    }

    /**
     * @return mixed
     */
    public function getSoldAt()
    {
        return $this->sold_at;
    }

    /**
     * @param mixed $sold_at
     */
    public function setSoldAt($sold_at): void
    {
        $this->sold_at = $sold_at;
    }



}
