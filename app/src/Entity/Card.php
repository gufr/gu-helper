<?php

namespace App\Entity;

use App\Repository\CardRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CardRepository::class)
 */
class Card
{

    CONST QUALITY = array(
        '4' => 'Meteorite',
        '3' => 'Shadow',
        '2' => 'Gold',
        '1' => 'Diamond',
    );

    CONST STATUS = array(
        '0' => 'Minted',
        '1' => 'Bought',
        '2' => 'Sold',
    );

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $quality;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $token_address;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $token_id;

    /**
     * @var UniqueCard
     * @ORM\ManyToOne(targetEntity="App\Entity\UniqueCard", inversedBy="cards", cascade={"persist"})
     */
    private $uniqueCard;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $buy_eth_price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $last_offer_price;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuality(): ?string
    {
        return $this->quality;
    }

    public function setQuality(string $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getTokenAddress(): ?string
    {
        return $this->token_address;
    }

    public function setTokenAddress(string $token_address): self
    {
        $this->token_address = $token_address;

        return $this;
    }

    public function getTokenId(): ?int
    {
        return $this->token_id;
    }

    public function setTokenId(int $token_id): self
    {
        $this->token_id = $token_id;

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return UniqueCard
     */
    public function getUniqueCard(): ?UniqueCard
    {
        return $this->uniqueCard;
    }

    /**
     * @param UniqueCard $uniqueCard
     */
    public function setUniqueCard(?UniqueCard $uniqueCard): void
    {
        $this->uniqueCard = $uniqueCard;
    }

    /**
     * @return mixed
     */
    public function getBuyEthPrice()
    {
        return $this->buy_eth_price;
    }

    /**
     * @param mixed $buy_eth_price
     */
    public function setBuyEthPrice($buy_eth_price): void
    {
        $this->buy_eth_price = $buy_eth_price;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLastOfferPrice()
    {
        return $this->last_offer_price;
    }

    /**
     * @param mixed $last_offer_price
     */
    public function setLastOfferPrice($last_offer_price): void
    {
        $this->last_offer_price = $last_offer_price;
    }

}
