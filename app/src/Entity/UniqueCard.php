<?php

namespace App\Entity;

use App\Repository\UniqueCardRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UniqueCardRepository::class)
 */
class UniqueCard
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $god;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $guset;

    /**
     * @ORM\Column(type="integer")
     */
    private $mana;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tribe;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $attack;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $effect;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $health;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $rarity;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Card", mappedBy="uniquecard", cascade={"persist","remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $cards;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGod(): ?string
    {
        return $this->god;
    }

    public function setGod(string $god): self
    {
        $this->god = $god;

        return $this;
    }

    public function getGuset(): ?string
    {
        return $this->guset;
    }

    public function setGuset(string $guset): self
    {
        $this->guset = $guset;

        return $this;
    }

    public function getMana(): ?int
    {
        return $this->mana;
    }

    public function setMana(int $mana): self
    {
        $this->mana = $mana;

        return $this;
    }

    public function getTribe(): ?string
    {
        return $this->tribe;
    }

    public function setTribe(string $tribe): self
    {
        $this->tribe = $tribe;

        return $this;
    }

    public function getAttack(): ?int
    {
        return $this->attack;
    }

    public function setAttack(?int $attack): self
    {
        $this->attack = $attack;

        return $this;
    }

    public function getEffect(): ?string
    {
        return $this->effect;
    }

    public function setEffect(string $effect): self
    {
        $this->effect = $effect;

        return $this;
    }

    public function getHealth(): ?int
    {
        return $this->health;
    }

    public function setHealth(?int $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getRarity(): ?string
    {
        return $this->rarity;
    }

    public function setRarity(string $rarity): self
    {
        $this->rarity = $rarity;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCards(): ?Collection
    {
        return $this->cards;
    }


}
