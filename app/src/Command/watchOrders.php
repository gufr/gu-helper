<?php

namespace App\Command;

use App\Entity\Sale;
use App\Entity\LastSale;
use App\Repository\LastSaleRepository;
use App\Repository\SaleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class watchOrders extends Command
{
    private $entityManager;
    private $saleRepository;
    private $lastSaleRepository;
    private $client;

    protected static $defaultName = 'gu:orders:watch';

    public function __construct(
        EntityManagerInterface $em,
        SaleRepository $saleRepository,
        LastSaleRepository $lastSaleRepository,
        HttpClientInterface $client
    )
    {
        parent::__construct();
        $this->entityManager        = $em;
        $this->saleRepository       = $saleRepository;
        $this->lastSaleRepository   = $lastSaleRepository;
        $this->client               = $client;
    }

    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Start Order watch',
            '=================',
            '',
        ]);
        $continue       = true;
        $cursor         = "";
        $nbTotalOrders  = 0;

        // url d'api pour la recherche des assets de la collection "Gods Unchained"
        date_default_timezone_set('UTC');
        $tenMinAgo = date("Y-m-d\TH:i:s.000\Z", mktime(date('H'), date('i')-15, date('s'), date("m")  , date("d"), date("Y")));

        while ($continue) {
            // url d'api pour la recherche des assets de la collection "Gods Unchained"
            $url = 'https://api.x.immutable.com/v1/orders?sell_token_address=0xacb3c6a43d15b907e8433077b6d38ae40936fe2c&sell_token_type=ERC721&buy_token_type=ETH&status=filled&order_by=updated_at&direction=asc&updated_min_timestamp='.$tenMinAgo;

            if ($cursor > "") $url .= '&cursor=' . $cursor;
            $response = $this->client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]);
            if ($response->getStatusCode() === 200) {
                $content = json_decode($response->getContent());
                $orders = $content->result;
                foreach ($orders as $order) {
                    if ($order->buy->type=='ETH') {
                        if (!($s = $this->saleRepository->find($order->order_id))) {
                            preg_match('/id=([0-9]*)&q=([0-9])/', $order->sell->data->properties->image_url, $matches);
                            if (isset($matches[1]) && isset($matches[2])) {
                                $proto = $matches[1];
                                $quality = $matches[2];
                                if ($proto > '' && is_numeric($proto) && $quality > '' && is_numeric($quality)) {
                                    $nbTotalOrders++;
                                    $s = new Sale();
                                    $s->setId($order->order_id);
                                    $s->setProto($proto);
                                    $s->setQuality($quality);
                                    $s->setDecimals($order->buy->data->decimals);
                                    $s->setEthPrice($order->buy->data->quantity);
                                    $date = new \DateTimeImmutable($order->updated_timestamp);
                                    $s->setSoldAt($date);
                                    $this->entityManager->persist($s);
                                    $this->entityManager->flush();
                                    $this->entityManager->clear();

                                    $lastsale = $this->lastSaleRepository->findOneBy(array('proto'=>$proto, 'quality'=>$quality)) ?: new LastSale();
                                    $lastsale->setId($order->order_id);
                                    $lastsale->setProto($proto);
                                    $lastsale->setQuality($quality);
                                    $lastsale->setDecimals($order->buy->data->decimals);
                                    $lastsale->setEthPrice($order->buy->data->quantity);
                                    $date = new \DateTimeImmutable($order->updated_timestamp);
                                    $lastsale->setSoldAt($date);
                                    $this->entityManager->persist($lastsale);
                                    $this->entityManager->flush();
                                    $this->entityManager->clear();

                                } else {
                                    $output->writeln("Card : " . $proto . " incorrect format");
                                }
                            }
                        } else {
                            $s->setEthPrice($order->buy->data->quantity);
                            $this->entityManager->persist($s);
                            $this->entityManager->flush();
                            $this->entityManager->clear();
                        }
                    }
                }
                $continue = $content->remaining;
                $cursor = $content->cursor;
            } else {
                return Command::FAILURE;
            }
        }
        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        $output->writeln("Purchase inserted: ".$nbTotalOrders);
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }
}
