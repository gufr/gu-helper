<?php

namespace App\Command;

use App\Entity\Card;
use App\Entity\UniqueCard;
use App\Repository\CardRepository;
use App\Repository\UniqueCardRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class updateCards extends Command
{
    private $entityManager;
    private $cardRepository;
    private $client;

    protected static $defaultName = 'gu:cards:update';

    public function __construct(
        EntityManagerInterface $em,
        CardRepository $cardRepository,
        UniqueCardRepository $uniqueCardRepository,
        HttpClientInterface $client
    )
    {
        parent::__construct();
        $this->entityManager        = $em;
        $this->cardRepository       = $cardRepository;
        $this->uniqueCardRepository = $uniqueCardRepository;
        $this->client               = $client;
    }

    protected function configure(): void
    {
        $this->addArgument('usrAddr', InputArgument::REQUIRED, 'The address of the user whose collection you want to update');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Start Card Update',
            '=================',
            '',
        ]);
        $usrAddr = $input->getArgument('usrAddr');
        // On supprime les cartes de l'utilisateur  (annule et remplace)
        $this->cardRepository->deleteByUser($usrAddr);

        $continue = true;
        $cursor = "";
        $nbTotalCards = 0;
        while ($continue) {
            // url d'api pour la recherche des assets de la collection "Gods Unchained"
            $url = 'https://api.x.immutable.com/v1/assets?collection=0xacb3c6a43d15b907e8433077b6d38ae40936fe2c&user='.$usrAddr;
            if ($cursor > "") $url .= '&cursor=' . $cursor;
            $response = $this->client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]);
            if ($response->getStatusCode()===200) {
                $content = json_decode($response->getContent());
                $assets = $content->result;
                foreach ($assets as $asset) {
                    // on ne prend que les cartes
                    if (isset($asset->metadata->type) && $asset->metadata->type == "card") {
                        $nbTotalCards++;
                        if (!$this->cardRepository->findOneBy(array("token_id" => $asset->token_id))){
                            $card = new Card();
                            $card->setTokenId($asset->token_id);
                            $card->setTokenAddress($asset->token_address);
                            $qualityNum = array_search($asset->metadata->quality, Card::QUALITY) ?: 0;
                            $card->setQuality($qualityNum);
                            $card->setUser($asset->user);
                            $date = new \DateTimeImmutable($asset->created_at);
                            $card->setCreatedAt($date);
                            $date = new \DateTimeImmutable($asset->updated_at);
                            $card->setUpdatedAt($date);
                            $card->setStatus(0);
                            $uniquecard = $this->uniqueCardRepository->findOneBy(array("id" => $asset->metadata->proto));
                            // si on a trouvé un nouveau modèle de carte
                            if (!$uniquecard) {
                                $uniquecard = new UniqueCard();
                                $uniquecard->setId($asset->metadata->proto);
                                $uniquecard->setName($asset->metadata->name);
                                $uniquecard->setGod($asset->metadata->god);
                                $uniquecard->setGuset($asset->metadata->set);
                                $uniquecard->setMana($asset->metadata->mana);
                                $uniquecard->setRarity($asset->metadata->rarity);
                                if (isset($asset->metadata->effect)) {
                                    $uniquecard->setEffect($asset->metadata->effect);
                                }
                                if (isset($asset->metadata->tribe)) {
                                    $uniquecard->setTribe($asset->metadata->tribe);
                                }
                                if (isset($asset->metadata->attack)) {
                                    $uniquecard->setAttack($asset->metadata->attack);
                                }
                                if (isset($asset->metadata->health)) {
                                    $uniquecard->setHealth($asset->metadata->health);
                                }
                            }
                            $card->setUniqueCard($uniquecard);
                            // on va chercher si elle a été achetée
                            $url_purchase = 'https://api.x.immutable.com/v1/orders?order_by=updated_at&direction=desc&page_size=1&buy_token_id='.$asset->token_id.'&user='.$usrAddr;

                            $r = $this->client->request('GET', $url_purchase, [
                                'headers' => [
                                    'Accept' => 'application/json',
                                ],
                            ]);
                            if ($r->getStatusCode()===200) {
                                $c = json_decode($r->getContent());
                                if (isset($c->result) && count($c->result)>0){
                                    $p = $c->result[0];
                                    $card->setStatus(1);
                                    $ethprice = $p->sell->data->quantity / pow(10, $p->sell->data->decimals);
                                    $card->setBuyEthPrice($ethprice);
                                }
                            }
                            $this->entityManager->persist($card);
                            $this->entityManager->flush();
                            $this->entityManager->clear();
                        }
                    }
                }
                $continue = $content->remaining;
                $cursor = $content->cursor;
            }else{
                return Command::FAILURE;
            }
        }

        $this->cardRepository->updateLastPrice($usrAddr);

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }
}
