<?php

namespace App\Command;

use App\Entity\Card;
use App\Entity\Order;
use App\Entity\UniqueCard;
use App\Repository\CardRepository;
use App\Repository\OrderRepository;
use App\Repository\UniqueCardRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class updateOrders extends Command
{
    private $entityManager;
    private $cardRepository;
    private $client;

    protected static $defaultName = 'gu:orders:update';

    public function __construct(
        EntityManagerInterface $em,
        UniqueCardRepository $uniqueCardRepository,
        OrderRepository $orderRepository,
        HttpClientInterface $client
    )
    {
        parent::__construct();
        $this->entityManager        = $em;
        $this->uniqueCardRepository = $uniqueCardRepository;
        $this->orderRepository      = $orderRepository;
        $this->client               = $client;
    }

    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Start Order Update',
            '=================',
            '',
        ]);
        $continue       = true;
        $cursor         = "";
        $nbTotalOrders  = 0;
        while ($continue) {
            // url d'api pour la recherche des assets de la collection "Gods Unchained"
            $url = 'https://api.x.immutable.com/v1/orders?status=active';
            if ($cursor > "") $url .= '&cursor=' . $cursor;
            $response = $this->client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]);
            if ($response->getStatusCode()===200) {
                $content = json_decode($response->getContent());
                $orders = $content->result;
                foreach ($orders as $order) {
                    // on ne prend que les ventes GU
                    // TODO : faire fonctionner le filtrer metadata de l'API
                    if (isset($order->sell->data->properties->collection->name)
                        && $order->sell->data->properties->collection->name == "Gods Unchained") {
                        if (!$this->orderRepository->find($order->order_id)) {
                            preg_match('/id=([0-9]*)&q=([0-9])/', $order->sell->data->properties->image_url, $matches);
                            if (isset($matches[1])) {
                                $cardId = $matches[1];
                                if ($cardId > '') {
                                    $quality = $matches[2];
                                    $nbTotalOrders++;
                                    $o = new Order();
                                    $o->setId($order->order_id);
                                    $o->setQuantity($order->sell->data->quantity);
                                    $uniquecard = $this->uniqueCardRepository->findOneBy(array("id" => $cardId));
                                    if ($uniquecard) {
                                        $o->setId($order->order_id);
                                        $o->setQuality($quality);
                                        $ethprice = $order->buy->data->quantity / pow(10, $order->buy->data->decimals);
                                        $o->setPrice($ethprice);
                                        $o->setImgurl($order->sell->data->properties->image_url);
                                        $date = new \DateTimeImmutable($order->timestamp);
                                        $o->setCreatedAt($date);
                                        $date = new \DateTimeImmutable($order->updated_timestamp);
                                        $o->setUpdatedAt($date);
                                        $o->setUniqueCard($uniquecard);
                                        $this->entityManager->persist($o);
                                        $this->entityManager->flush();
                                        $this->entityManager->clear();
                                    }else{
                                        $output->writeln("Card : ".$cardId." not found");
                                    }
                                }
                            }
                        }
                    }
                }
                $continue = $content->remaining;
                $cursor = $content->cursor;
                $output->writeln($cursor);
            }else{
                return Command::FAILURE;
            }
        }
        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        $output->writeln("Orders updated: ".$nbTotalOrders);
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }
}
