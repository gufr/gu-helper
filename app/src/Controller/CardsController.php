<?php

namespace App\Controller;

use App\Entity\Card;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class CardsController extends AbstractController
{
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Route("/cards", name="cards")
     */
    public function index(ManagerRegistry $doctrine): Response
    {
        $usrAddr = '0x281F938ACBA88857F790f8755358C2F5dfB200b0';
        //TODO conditionner l'appel à l'update
        $this->update($usrAddr);
        $cards = $doctrine->getRepository(Card::class)->findBy(array('user'=>$usrAddr));

        return $this->render('cards/index.html.twig', [
            'cards' => $cards,
        ]);
    }

    /**
     *
     */
    private function update($usrAddr): void
    {
        // on met à jour les cartes de l'utilisateur
        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput([
            'command' => 'gu:cards:update',
            'usrAddr' => $usrAddr
        ]);
        $output = new BufferedOutput();
        $application->run($input, $output);
    }
}
