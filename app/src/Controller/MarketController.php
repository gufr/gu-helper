<?php

namespace App\Controller;

use App\Entity\Sale;
use Doctrine\Persistence\ManagerRegistry;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Column\MapColumn;
use Omines\DataTablesBundle\Column\NumberColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\DataTableFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MarketController extends AbstractController
{
    #[Route('/market', name: 'market')]
    public function index(HttpClientInterface $client, ManagerRegistry $doctrine, Request $request, DataTableFactory $dataTableFactory): Response
    {
        $continue       = true;
        $cursor         = "";
        $nbTotalOrders  = 0;
        $arrOfOrders   = array();
        while ($continue) {
            date_default_timezone_set('UTC');
            $tenMinAgo = date("Y-m-d\TH:i:s.000\Z", mktime(date('H'), date('i')-5, date('s'), date("m")  , date("d"), date("Y")));
            // url d'api pour la recherche des assets de la collection "Gods Unchained"
            $url = 'https://api.x.immutable.com/v1/orders?sell_token_address=0xacb3c6a43d15b907e8433077b6d38ae40936fe2c&sell_token_type=ERC721&buy_token_type=ETH&status=active&updated_min_timestamp='.$tenMinAgo;

            if ($cursor > "") $url .= '&cursor=' . $cursor;
            $response = $client->request('GET', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]);

            if ($response->getStatusCode()===200) {
                $content = json_decode($response->getContent());
                $orders = $content->result;
                foreach ($orders as $order) {
                    // on ne prend que les ventes GU
                    // TODO : faire fonctionner le filtrer metadata de l'API
                    if (isset($order->sell->data->properties->collection->name)
                        && $order->sell->data->properties->collection->name == "Gods Unchained") {
                        preg_match('/id=([0-9]*)&q=([0-9])/', $order->sell->data->properties->image_url, $matches);
                        if (isset($matches[1])) {
                            $proto = $matches[1];
                            if ($proto > '') {
                                $quality    = $matches[2];
                                $ethprice   = $order->buy->data->quantity / pow(10, $order->buy->data->decimals);
                                $avg        = $doctrine->getRepository(Sale::class)->getAvgPriceForProtoAndQuality($proto, $quality);
                                if ($avg>0) {
                                    $diffabs    = $avg - $ethprice;
                                    $diff       = $diffabs / $avg * 100;
                                    $cardId     = $proto . '-' . $quality;
                                    if ($diff > 20) {
                                        if (!isset($arrOfOrders[$cardId]['diff']) || (isset($arrOfOrders[$cardId]['diff']) && $arrOfOrders[$cardId]['diff'] > $diff)) {
                                            $arrOfOrders[$cardId]['proto'] = $proto;
                                            $arrOfOrders[$cardId]['quality'] = $quality;
                                            $arrOfOrders[$cardId]['url'] = 'https://tokentrove.com/collection/GodsUnchainedCards/' . $cardId;
                                            $arrOfOrders[$cardId]['imgurl'] = $order->sell->data->properties->image_url;
                                            $arrOfOrders[$cardId]['cardname'] = $order->sell->data->properties->name;
                                            $arrOfOrders[$cardId]['ethprice'] = number_format($ethprice, 6);
                                            $arrOfOrders[$cardId]['avg'] = number_format($avg, 6);
                                            $arrOfOrders[$cardId]['diff'] = number_format($diff, 2);
                                            $arrOfOrders[$cardId]['diffabs'] = number_format($diffabs, 6);
                                            $arrOfOrders[$cardId]['updated_at'] = $order->updated_timestamp;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $continue = $content->remaining;
                $cursor = $content->cursor;
            }else{
                $msg = 'failure';
            }
        }
        return $this->render('market/index.html.twig', [
            'controller_name'   => 'MarketController',
            'arrOfOrders'         => $arrOfOrders
        ]);
    }
}
